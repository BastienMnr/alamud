from .event import Event2

class EnleverEvent(Event2):
    NAME = "enlever"

    def perform(self):
        if not self.object.has_prop("equiper"):
            return self.enlever_failed()
        self.inform("enlever")

    def enlever_failed(self):
        self.fail()
        self.inform("enlever.failed")