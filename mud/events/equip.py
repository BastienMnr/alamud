from .event import Event2

class EquipEvent(Event2):
    NAME = "equip"

    def perform(self):
        if not self.object.has_prop("equipable"):
            return self.equip_failed()
        for equipement in self.actor.contents():
            if equipement.has_prop("equiper"):
                return self.inform("equip.deja-equiper")
        self.inform("equip")

    def equip_failed(self):
        self.fail()
        self.inform("equip.failed")
