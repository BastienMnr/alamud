from .action import Action2
from mud.events import EnleverEvent

class EnleverAction(Action2):
    EVENT = EnleverEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "enlever"